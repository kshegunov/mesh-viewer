#ifndef SETUPLIGHTDIALOG_H
#define SETUPLIGHTDIALOG_H

#include <QDialog>

namespace Ui {
	class SetupLightDialog;
}

class QColor;

class SetupLightDialog : public QDialog
{
	Q_OBJECT

public:
	explicit SetupLightDialog(QWidget *parent = 0);
	virtual ~SetupLightDialog();

	void setAmbientColor(const QColor &);
	void setSpecularColor(const QColor &);
	void setDiffuseColor(const QColor &);

	QColor ambientColor() const;
	QColor specularColor() const;
	QColor diffuseColor() const;

private slots:
	void ambientColorChanged();
	void specularColorChanged();
	void diffuseColorChanged();

private:
	Ui::SetupLightDialog * ui;
};

#endif // SETUPLIGHTDIALOG_H
