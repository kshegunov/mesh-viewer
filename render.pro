#-------------------------------------------------
#
# Project created by QtCreator 2013-06-15T12:40:18
#
#-------------------------------------------------

MAKEFILE = render.make

QT += core gui widgets
CONFIG += link_prl

INCLUDEPATH += ../qogre ../qogre/QtOgre
LIBS += -L. -lqogre

TARGET = render
TEMPLATE = app

target.depends = qogre

win32  {
	DESTDIR = $$OUT_PWD
}

SOURCES +=\
	setuplightdialog.cpp \
    rendermainwindow.cpp \
    rendermain.cpp

HEADERS  += \
	setuplightdialog.h \
    rendermainwindow.h

FORMS    += \
	setuplight.ui \
    rendermainwindow.ui

RESOURCES +=
