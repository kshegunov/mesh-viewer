#include <QApplication>
#include <QString>

#include <QOgreEngine>
#include <QOgreScene>

#include "rendermainwindow.h"

int main(int argc, char *argv[])
{
	QApplication application(argc, argv);
	QOgreEngine ogre(application);

	RenderMainWindow window;
	window.showMaximized();

	return application.exec();
}

