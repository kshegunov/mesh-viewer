#ifndef RENDERMAINWINDOW_H
#define RENDERMAINWINDOW_H

#include <QMainWindow>

extern const int EXIT_AND_RESTART;

namespace Ui {
	class RenderMainWindow;
}

class QOgreMesh;
class QOgreScene;
class QOgreCamera;
class QOgreDirectionalLight;

class RenderMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit RenderMainWindow(QWidget *parent = 0);
	virtual ~RenderMainWindow();

public slots:
	void loadModel();
	void unloadModel();
	void rotateX(int);
	void rotateY(int);
	void rotateZ(int);
	void rotateReset();
	void zoom(int);
	void zoomReset();
	void setupScene();
	void setupLight();

private:
	void updateGL();

	Ui::RenderMainWindow * ui;

	QOgreCamera * camera;
	QOgreScene * scene;
	QOgreMesh * mesh;
	QOgreDirectionalLight * light;
};

#endif // RENDERMAINWINDOW_H
