#include "rendermainwindow.h"
#include "ui_rendermainwindow.h"
#include "setuplightdialog.h"

#include <cmath>

#include <QColor>
#include <QVector3D>
#include <QFileDialog>
#include <QMessageBox>
#include <QMatrix4x4>

#include <QOgreCamera>
#include <QOgreScene>
#include <QOgreMesh>
#include <QOgreDirectionalLight>
#include <QOgrePosition>
#include <QOgreException>
#include <QOgreOrientation>
#include <QOgreEnvelope>
#include <QOgreSize>
#include <QOgreEngine>

inline qreal pi()
{
	return 3.14159265359L;
}

QVector3D isometricProjection(const QVector3D & vector)
{
	static QMatrix4x4 isometric;
	if (isometric.isIdentity())  {
		qreal sqrt_3 = ::sqrt(3.0l), sqrt_2 = ::sqrt(2.0l);
		isometric(0, 0) = sqrt_3;
		isometric(0, 2) = -sqrt_3;
		isometric(1, 0) = 1;
		isometric(1, 1) = 2;
		isometric(1, 2) = 1;
		isometric(2, 0) = sqrt_2;
		isometric(2, 1) = -sqrt_2;
		isometric(2, 2) = sqrt_2;
	}

	return isometric.map(vector) * 0.5;
}

RenderMainWindow::RenderMainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::RenderMainWindow), mesh(NULL), light(NULL)
{
	ui->setupUi(this);

	QOgreEngine * ogre = QOgreEngine::instance();
	ogre->addPluginDirectory("Ogre3D");
	ogre->addResourceLocation("resources");
	ogre->loadPlugin("RenderSystem_GL_d");
	ogre->loadPlugin("RenderSystem_Direct3D9_d");

	scene = new QOgreGenericScene();
	QObject::connect(scene, SIGNAL(ready(QOgreScene *)), this, SLOT(setupScene()));
}

RenderMainWindow::~RenderMainWindow()
{
	delete ui;
}

void RenderMainWindow::loadModel()
{
	QString file = QFileDialog::getOpenFileName(this, "Select a mesh file ...", "./resources", "*.mesh");
	if (file.isEmpty())
		return;

	QString location = QFileInfo(file).absoluteDir().path();
	QOgreEngine::instance()->addResourceLocation(location);

	try  {
		unloadModel();

		mesh = new QOgreMesh(QFileInfo(file).fileName());
		QOgreEnvelope envelope(mesh->envelope());
		QOgreSize size(envelope.size());
		qreal maxSize = 1.5 * size.max();

		QOgreCamera * front = ui->front->camera();
		front->position().change(0, 0, maxSize);
		front->lookAt(envelope.center());

		QOgreCamera * top = ui->top->camera();
		top->position().change(0.1, maxSize, 0);
		top->lookAt(envelope.center());

		QOgreCamera * right = ui->right->camera();
		right->position().change(maxSize, 0, 0);
		right->lookAt(envelope.center());

		QOgreCamera * perspective = ui->perspective->camera();
		perspective->position().change(isometricProjection(QVector3D(maxSize, 0, 0)));
		perspective->lookAt(envelope.center());


		ui->meshSize->setText(QString("X size: %1   Y size: %2   Z size: %3").arg(size.width()).arg(size.height()).arg(size.depth()));
		ui->xRotate->setEnabled(true);
		ui->yRotate->setEnabled(true);
		ui->zRotate->setEnabled(true);
		ui->resetRotate->setEnabled(true);
		ui->cameraZoom->setEnabled(true);
		ui->resetZoom->setEnabled(true);

		updateGL();
	}
	catch (QOgreException e)
	{
		QMessageBox::warning(this, "Warning", "An error occured while loading the mesh");
		QMessageBox::warning(this, "Ogre", e.description());
	}
	catch (...)
	{
		QMessageBox::warning(this, "Warning", "An error occure while loading the mesh");
	}
}

void RenderMainWindow::unloadModel()
{
	ui->meshSize->setText("");
	ui->xRotate->setEnabled(false);
	ui->yRotate->setEnabled(false);
	ui->zRotate->setEnabled(false);
	ui->resetRotate->setEnabled(false);
	ui->cameraZoom->setEnabled(false);
	ui->resetZoom->setEnabled(false);

	rotateReset();
	zoomReset();

	delete mesh;
	mesh = NULL;

	updateGL();
}

void RenderMainWindow::rotateX(int position)
{
	qreal range = ui->xRotate->maximum() - ui->xRotate->minimum(), angle = (position - ui->xRotate->minimum()) / range - 0.5;
	QQuaternion orientation(mesh->orientation());
	orientation.setX(angle * pi() / 2);
	mesh->orientation().change(orientation);

	updateGL();
}

void RenderMainWindow::rotateY(int position)
{
	qreal range = ui->xRotate->maximum() - ui->xRotate->minimum(), angle = (position - ui->xRotate->minimum()) / range - 0.5;
	QQuaternion orientation = mesh->orientation();
	orientation.setY(angle * pi() / 2);
	mesh->orientation().change(orientation);

	updateGL();
}

void RenderMainWindow::rotateZ(int position)
{
	qreal range = ui->xRotate->maximum() - ui->xRotate->minimum(), angle = (position - ui->xRotate->minimum()) / range - 0.5;
	QQuaternion orientation = mesh->orientation();
	orientation.setZ(angle * pi() / 2);
	mesh->orientation().change(orientation);

	updateGL();
}

void RenderMainWindow::rotateReset()
{
	qreal position = (ui->xRotate->maximum() - ui->xRotate->minimum()) / 2;
	ui->xRotate->setValue(position);
	ui->yRotate->setValue(position);
	ui->zRotate->setValue(position);

	updateGL();
}

void RenderMainWindow::zoom(int position)
{
	qreal range = ui->cameraZoom->maximum() - ui->cameraZoom->minimum(), scale = (position - ui->cameraZoom->minimum()) / range * 2;
	if (qFuzzyIsNull(scale))
		scale = 0.05;

	scale = 1.5 * mesh->envelope().size().max() * scale;

	ui->front->camera()->position().change(0, 0, scale);
	ui->top->camera()->position().change(0, scale, 0);
	ui->right->camera()->position().change(scale, 0, 0);
	ui->perspective->camera()->position().change(isometricProjection(QVector3D(scale, 0, 0)));

	updateGL();
}

void RenderMainWindow::zoomReset()
{
	ui->cameraZoom->setValue((ui->cameraZoom->minimum() + ui->cameraZoom->maximum()) / 2);
}

void RenderMainWindow::updateGL()
{
	ui->front->update();
	ui->top->update();
	ui->right->update();
	ui->perspective->update();
}

void RenderMainWindow::setupScene()
{
	ui->front->setCamera(new QOgreCamera(scene));
	ui->top->setCamera(new QOgreCamera(scene));
	ui->right->setCamera(new QOgreCamera(scene));
	ui->perspective->setCamera(new QOgreCamera(scene));

	scene->setAmbientLightColor(QColor(64, 64, 64));
	scene->setShadowType(QtOgre::AdditiveStencilShadow);

	light = new QOgreDirectionalLight(scene);
	light->setSpecularColor(QColor(128, 128, 96));
	light->setDiffuseColor(QColor(128, 128, 96));
	light->setDirection(QVector3D(-1, -1, -1));
}


void RenderMainWindow::setupLight()
{
	QOgreScene * scene = QOgreScene::scene();
	Q_ASSERT(scene && light);

	SetupLightDialog dialog(this);
	dialog.setAmbientColor(scene->ambientLightColor());
	dialog.setSpecularColor(light->specularColor());
	dialog.setDiffuseColor(light->diffuseColor());

	if (dialog.exec() == QDialog::Accepted)  {
		scene->setAmbientLightColor(dialog.ambientColor());
		light->setSpecularColor(dialog.specularColor());
		light->setDiffuseColor(dialog.diffuseColor());

		updateGL();
	}
}

