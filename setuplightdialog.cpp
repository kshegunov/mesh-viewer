#include "setuplightdialog.h"
#include "ui_setuplight.h"

#include <QPalette>

SetupLightDialog::SetupLightDialog(QWidget * parent)
	: QDialog(parent), ui(new Ui::SetupLightDialog)
{
	ui->setupUi(this);
}

SetupLightDialog::~SetupLightDialog()
{
	delete ui;
}

void SetupLightDialog::setAmbientColor(const QColor & color)
{
	ui->ambientRed->setValue(color.red());
	ui->ambientGreen->setValue(color.green());
	ui->ambientBlue->setValue(color.blue());

	ambientColorChanged();
}

void SetupLightDialog::setSpecularColor(const QColor & color)
{
	ui->specularRed->setValue(color.red());
	ui->specularGreen->setValue(color.green());
	ui->specularBlue->setValue(color.blue());

	specularColorChanged();
}

void SetupLightDialog::setDiffuseColor(const QColor & color)
{
	ui->diffuseRed->setValue(color.red());
	ui->diffuseGreen->setValue(color.green());
	ui->diffuseBlue->setValue(color.blue());

	diffuseColorChanged();
}

QColor SetupLightDialog::ambientColor() const
{
	return QColor(ui->ambientRed->value(), ui->ambientGreen->value(), ui->ambientBlue->value());
}

QColor SetupLightDialog::specularColor() const
{
	return QColor(ui->specularRed->value(), ui->specularGreen->value(), ui->specularBlue->value());
}

QColor SetupLightDialog::diffuseColor() const
{
	return QColor(ui->diffuseRed->value(), ui->diffuseGreen->value(), ui->diffuseBlue->value());
}

void SetupLightDialog::ambientColorChanged()
{
	QPalette palette(ui->ambientPreview->palette());
	palette.setColor(QPalette::Background, ambientColor());
	ui->ambientPreview->setPalette(palette);
	ui->ambientPreview->update();
}

void SetupLightDialog::specularColorChanged()
{
	QPalette palette(ui->specularPreview->palette());
	palette.setColor(QPalette::Background, specularColor());
	ui->specularPreview->setPalette(palette);
	ui->specularPreview->update();
}

void SetupLightDialog::diffuseColorChanged()
{
	QPalette palette(ui->diffusePreview->palette());
	palette.setColor(QPalette::Background, diffuseColor());
	ui->diffusePreview->setPalette(palette);
	ui->diffusePreview->update();
}
